<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Booked rooms:</h2>
	 ${bookedRooms}
	 <h2>Available rooms:</h2>
	 ${availableRooms}
	 <h2>Room by number:</h2>
	 ${room}
	 <h2>Rooms by price:</h2>
	 ${priceRooms}
</body>
</html>