package com.epam.booking.service;

import java.util.List;

import com.epam.booking.dto.GuestDTO;

public interface GuestService {
	List<GuestDTO> findaAll();
	GuestDTO findGuest(int id);
}
