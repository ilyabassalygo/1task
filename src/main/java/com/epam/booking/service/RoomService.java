package com.epam.booking.service;

import java.util.List;

import com.epam.booking.dto.RoomDTO;

public interface RoomService {
	 List<RoomDTO> findAllRooms();
	 List<RoomDTO> findNotBookedRooms();
	 List<RoomDTO> findBookedRooms();
	 List<RoomDTO> findByPrice(int price);
	 RoomDTO findByNumber(int roomNumber);
}
