package com.epam.booking.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.booking.dao.GuestDAO;
import com.epam.booking.dto.GuestDTO;
import com.epam.booking.entity.Guest;
import com.epam.booking.exception.DAOException;
import com.epam.booking.util.BookingMapper;

public class GuestServiceImpl implements GuestService {
	private static final Logger LOGGER = LogManager.getRootLogger();
	private GuestDAO guestDAO;

	public void setGuestDAO(GuestDAO guestDAO) {
		this.guestDAO = guestDAO;
	}


	public List<GuestDTO> findaAll() {
		List<GuestDTO> gList = new ArrayList<>();
		try {
			for (Guest guest: guestDAO.findAll()) {
				gList.add(BookingMapper.mapGuest(guest));
			}
		} catch (DAOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
		return gList;
	}


	@Override
	public GuestDTO findGuest(int id) {
		try {
			return BookingMapper.mapGuest(guestDAO.find(id));
		} catch (DAOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			return null;
		}
	}
	
	
}
