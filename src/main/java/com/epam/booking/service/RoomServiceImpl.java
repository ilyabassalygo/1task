package com.epam.booking.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.booking.dao.RoomDAO;
import com.epam.booking.dto.RoomDTO;
import com.epam.booking.entity.Room;
import com.epam.booking.exception.DAOException;
import com.epam.booking.util.BookingMapper;

public class RoomServiceImpl implements RoomService {
	private static final Logger LOGGER = LogManager.getRootLogger();
	
	private RoomDAO roomDAO;
	
	public void setRoomDAO(RoomDAO roomDAO) {
		this.roomDAO = roomDAO;
	}

	@Override
	public List<RoomDTO> findAllRooms() {
		List<RoomDTO> rList = new ArrayList<>();
		try {
			for (Room room: roomDAO.findAll()) {
				rList.add(BookingMapper.mapRoom(room));
			}
		} catch (DAOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
		return rList;
	}

	@Override
	public List<RoomDTO> findNotBookedRooms() {
		List<RoomDTO> rList = new ArrayList<>();
		try {
			for (Room room: roomDAO.findAll()) {
				if (room.getBookedRooms().isEmpty()) {
					rList.add(BookingMapper.mapRoom(room));
				}
			}
		} catch (DAOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
		return rList;
	}

	@Override
	public List<RoomDTO> findBookedRooms() {
		List<RoomDTO> rList = new ArrayList<>();
		try {
			for (Room room: roomDAO.findAll()) {
				if (!room.getBookedRooms().isEmpty()) {
					rList.add(BookingMapper.mapRoom(room));
				}
			}
		} catch (DAOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
		return rList;
	}

	@Override
	public List<RoomDTO> findByPrice(int price) {
		final int delta = 10;
		int tempPrice = 0;
		List<RoomDTO> rooms = new ArrayList<>();
		for (RoomDTO room : this.findAllRooms()) {
			tempPrice = room.getPrice();
			if (tempPrice >= 0 && tempPrice >= (price - delta) && tempPrice <= (price + delta)) {
				rooms.add(room);
			}
		}
		return rooms;
	}

	@Override
	public RoomDTO findByNumber(int roomNumber) {
		try {
			return BookingMapper.mapRoom(roomDAO.find(roomNumber));
		} catch (DAOException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
			return null;
		}
	}

}
