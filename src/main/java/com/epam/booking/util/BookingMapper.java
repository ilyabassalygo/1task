package com.epam.booking.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.epam.booking.dto.BookedRoomDTO;
import com.epam.booking.dto.GuestDTO;
import com.epam.booking.dto.RoomDTO;
import com.epam.booking.entity.BookedRoom;
import com.epam.booking.entity.Guest;
import com.epam.booking.entity.Room;

public class BookingMapper {

	//converts booked room to booked room dto and returns set of BookedRoomDTO
	private static Set<BookedRoomDTO> defineRooms(Set<BookedRoom> rooms) {
		Iterator<BookedRoom> iterator = rooms.iterator();
		
		Set<BookedRoomDTO> roomDTOs = new HashSet<>();
		while(iterator.hasNext()) {
			roomDTOs.add(BookingMapper.mapBookedRoom(iterator.next()));
		}
		return roomDTOs;
	}

	//used to map booked room takes guest name and room number
	public static BookedRoomDTO mapBookedRoom(BookedRoom bookedRoom) {
		BookedRoomDTO bookedRoomDTO = new BookedRoomDTO();
		bookedRoomDTO.setId(bookedRoom.getId());
		bookedRoomDTO.setRoom(bookedRoom.getRoom().getRoomNumber());
		bookedRoomDTO.setGuest(bookedRoom.getGuest().getName());
		bookedRoomDTO.setBookingDate(bookedRoom.getBookingDate());
		bookedRoomDTO.setExpiredDate(bookedRoom.getExpiredDate());
		return bookedRoomDTO;
	}
	
	public static GuestDTO mapGuest(Guest guest) {
		GuestDTO guestDTO = new GuestDTO();
		guestDTO.setId(guest.getId());
		guestDTO.setName(guest.getName());
		guestDTO.setPassportId(guest.getPassportId());
		guestDTO.setRooms(defineRooms(guest.getRooms()));
		return guestDTO;
	}
	
	public static RoomDTO mapRoom(Room room) {
		RoomDTO roomDTO = new RoomDTO();
		roomDTO.setRoomNumber(room.getRoomNumber());
		roomDTO.setPrice(room.getPrice());
		roomDTO.setBookedRooms(defineRooms(room.getBookedRooms()));
		return roomDTO;
	}
}
