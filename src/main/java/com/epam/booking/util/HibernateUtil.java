package com.epam.booking.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

//no more used this and hibernate.cfg.xml, changed with spring config and dependency injection
public class HibernateUtil {
	private static final Logger LOGGER = LogManager.getRootLogger();

	private static SessionFactory sessionFactory;

	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			Configuration configuration = new Configuration();
			/*
			configuration.addResource("com/epam/booking/entity/bookedroom.hbm.xml");
			configuration.addResource("com/epam/booking/entity/guest.hbm.xml");
			configuration.addResource("com/epam/booking/entity/room.hbm.xml");*/
			configuration.configure("hibernate.cfg.xml");
			LOGGER.log(Level.INFO, "Hibernate Configuration loaded");

			ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
					.applySettings(configuration.getProperties()).build();
			LOGGER.log(Level.INFO, "Hibernate serviceRegistry created");

			sessionFactory = configuration.buildSessionFactory(serviceRegistry);

			return sessionFactory;
		} catch (Throwable ex) {
			LOGGER.log(Level.ERROR, "Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = buildSessionFactory();
		}
		return sessionFactory;
	}

}
