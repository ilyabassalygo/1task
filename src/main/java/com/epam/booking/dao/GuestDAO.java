package com.epam.booking.dao;

import java.util.List;

import com.epam.booking.entity.Guest;
import com.epam.booking.exception.DAOException;


public interface GuestDAO {
	List<Guest> findAll() throws DAOException;
	Guest find(int id) throws DAOException;
	void save(Guest guest) throws DAOException;
	Guest update(Guest guest) throws DAOException;
	Guest remove(int id) throws DAOException;
}
