package com.epam.booking.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.epam.booking.entity.BookedRoom;
import com.epam.booking.exception.DAOException;

public class BookedRoomDAOImpl implements BookedRoomDAO {
	private SessionFactory sessionFactory;
	private static final String SELECT_BOOKED_ROOMS = "from BookedRoom";

	// setter method injection
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<BookedRoom> findAll() throws DAOException {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Query<BookedRoom> query = session.createQuery(SELECT_BOOKED_ROOMS);
			List<BookedRoom> bookedRooms = query.getResultList();
			for (BookedRoom bookedRoom : bookedRooms) {
				Hibernate.initialize(bookedRoom.getRoom());
			}
			for (BookedRoom bookedRoom : bookedRooms) {
				Hibernate.initialize(bookedRoom.getGuest());
			}
			return bookedRooms;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while find all in BookedRoomImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public BookedRoom find(int id) throws DAOException {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			BookedRoom bookedRoom = session.load(BookedRoom.class, id);
			Hibernate.initialize(bookedRoom.getGuest());
			Hibernate.initialize(bookedRoom.getRoom());
			return bookedRoom;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while find(id) in BookedRoomImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void save(BookedRoom bookedRoom) throws DAOException {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.save(bookedRoom);
			Hibernate.initialize(bookedRoom.getGuest());
			Hibernate.initialize(bookedRoom.getRoom());
			tx.commit();
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while save in BookedRoomImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	@Override
	public BookedRoom update(BookedRoom bookedRoom) throws DAOException {
		Session session = null;
		try {
			session = this.sessionFactory.openSession();
			Transaction tx = session.beginTransaction();
			session.update(bookedRoom);
			Hibernate.initialize(bookedRoom.getGuest());
			Hibernate.initialize(bookedRoom.getRoom());
			tx.commit();
			return bookedRoom;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while update in BookedRoomImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public BookedRoom remove(int id) throws DAOException {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		BookedRoom bookedRoom = session.load(BookedRoom.class, id);
		Hibernate.initialize(bookedRoom.getGuest());
		Hibernate.initialize(bookedRoom.getRoom());
		session.remove(bookedRoom);
		tx.commit();
		return bookedRoom;
		}  catch (HibernateException e) {
			throw new DAOException("Hibernate exception while remove in BookedRoomImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
