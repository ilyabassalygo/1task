package com.epam.booking.dao;

import java.util.List;

import com.epam.booking.entity.Room;
import com.epam.booking.exception.DAOException;


public interface RoomDAO {
	List<Room> findAll() throws DAOException;
	Room find(int id) throws DAOException;
	Room save(Room room) throws DAOException;
	Room update(Room room) throws DAOException;
	Room remove(int id) throws DAOException;
}
