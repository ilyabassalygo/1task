package com.epam.booking.dao;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.epam.booking.entity.BookedRoom;
import com.epam.booking.entity.Room;
import com.epam.booking.exception.DAOException;

public class RoomDAOImpl implements RoomDAO {
	
	private SessionFactory sessionFactory;
	private static final String SELECT_ROOMS = "from Room";

	// setter method injection
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Room> findAll() throws DAOException  {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Query<Room> query = session.createQuery(SELECT_ROOMS);
		List<Room> rooms = query.getResultList();
		for (Room room: rooms) {
			Hibernate.initialize(room.getBookedRooms());
			Iterator<BookedRoom> iterator = room.getBookedRooms().iterator();
			while(iterator.hasNext()) {
				Hibernate.initialize(iterator.next().getGuest());
			}
		}
		session.close();
		return rooms;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while findAll in RoomDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Room find(int id) throws DAOException  {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Room room = session.load(Room.class, id);		
		Hibernate.initialize(room.getBookedRooms());
		Iterator<BookedRoom> iterator = room.getBookedRooms().iterator();
		while(iterator.hasNext()) {
			Hibernate.initialize(iterator.next().getGuest());
		}
		return room;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while find(id) in RoomDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Room save(Room room) throws DAOException  {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(room);
		Hibernate.initialize(room.getBookedRooms());
		Iterator<BookedRoom> iterator = room.getBookedRooms().iterator();
		while(iterator.hasNext()) {
			Hibernate.initialize(iterator.next().getGuest());
		}
		tx.commit();
		return room;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while save in RoomDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Room update(Room room) throws DAOException  {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(room);
		Hibernate.initialize(room.getBookedRooms());
		Iterator<BookedRoom> iterator = room.getBookedRooms().iterator();
		while(iterator.hasNext()) {
			Hibernate.initialize(iterator.next().getGuest());
		}
		tx.commit();
		return room;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while update in RoomDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Room remove(int id) throws DAOException  {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Room room = session.load(Room.class, id);
		session.remove(room);
		Hibernate.initialize(room.getBookedRooms());
		Iterator<BookedRoom> iterator = room.getBookedRooms().iterator();
		while(iterator.hasNext()) {
			Hibernate.initialize(iterator.next().getGuest());
		}
		tx.commit();
		return room;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while remove in RoomDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
