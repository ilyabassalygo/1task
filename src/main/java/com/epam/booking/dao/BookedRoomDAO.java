package com.epam.booking.dao;

import java.util.List;

import com.epam.booking.entity.BookedRoom;
import com.epam.booking.exception.DAOException;

public interface BookedRoomDAO {
	List<BookedRoom> findAll() throws DAOException;
	BookedRoom find(int id) throws DAOException;
	void save(BookedRoom bookedRoom) throws DAOException;
	BookedRoom update(BookedRoom bookedRoom) throws DAOException;
	BookedRoom remove(int id) throws DAOException;
}
