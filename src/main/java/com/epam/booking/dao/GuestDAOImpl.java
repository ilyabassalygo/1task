package com.epam.booking.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.epam.booking.entity.Guest;
import com.epam.booking.exception.DAOException;
public class GuestDAOImpl implements GuestDAO {
	private SessionFactory sessionFactory;
	private static final String SELECT_GUESTS = "from Guest";

	// setter method injection
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public List<Guest> findAll() throws DAOException {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Query<Guest> query = session.createQuery(SELECT_GUESTS);
		List<Guest> guests = query.getResultList();
		for(Guest guest: guests) {
			Hibernate.initialize(guest.getRooms());
		}
		return guests;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while findAll in GuestDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Guest find(int id) throws DAOException {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Guest guest = session.load(Guest.class, id);
		Hibernate.initialize(guest.getRooms());
		return guest;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while findAll in GuestDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public void save(Guest guest) throws DAOException {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(guest);
		Hibernate.initialize(guest.getRooms());
		tx.commit();
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while findAll in GuestDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Guest update(Guest guest) throws DAOException {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(guest);
		Hibernate.initialize(guest.getRooms());
		tx.commit();
		return guest;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while findAll in GuestDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	@Override
	public Guest remove(int id) throws DAOException {
		Session session = null;
		try {
		session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		Guest guest = session.load(Guest.class, id);
		Hibernate.initialize(guest.getRooms());
		session.remove(guest);
		tx.commit();
		return guest;
		} catch (HibernateException e) {
			throw new DAOException("Hibernate exception while findAll in GuestDAOImpl " + e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
