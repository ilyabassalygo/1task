package com.epam.booking.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.booking.dto.RoomDTO;
import com.epam.booking.service.RoomService;
import com.google.gson.Gson;

/**
 * Also can use this response.setContentType("application/json"); PrintWriter
 * writer = response.getWriter(); writer.println(roomJSON); writer.close();
 */
@WebServlet("/room/*")
public class Controller extends HttpServlet {
	private static final Logger LOGGER = LogManager.getRootLogger();

	private static final long serialVersionUID = 111122223333L;
	private static final String CONTEXT_PATH = "spring.xml";
	private static final String URI_REG = "\\/booking\\/room\\/[0-9]+";
	private static final String ID_CUT_REG = "[A-z\\\\/]+";
	private static final String RESULT_PAGE = "/result.jsp";

	ApplicationContext applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Gson gson = new Gson();

		RoomService roomService = applicationContext.getBean(RoomService.class);
		String uri = request.getRequestURI();

		try {
			int price = 0;
			if (request.getParameter("price") != null) {
				price = Integer.parseInt(request.getParameter("price"));
			}

			if (price != 0) {
				List<RoomDTO> rooms = roomService.findByPrice(price);
				String roomsJSON = gson.toJson(rooms);
				request.setAttribute("priceRooms", roomsJSON);
			}

			Pattern pattern = Pattern.compile(URI_REG);
			Matcher matcher = pattern.matcher(uri);

			if (matcher.matches()) {
				int id = Integer.parseInt(uri.replaceAll(ID_CUT_REG, ""));
				RoomDTO room = roomService.findByNumber(id);
				if (room != null) {
					String roomJSON = gson.toJson(room);
					request.setAttribute("room", roomJSON);
				} else {
					request.setAttribute("room", "No such room");
				}

			} else {
				List<RoomDTO> bookedRooms = roomService.findBookedRooms();
				List<RoomDTO> availabeRooms = roomService.findNotBookedRooms();
				String bookedRoomsJSON = gson.toJson(bookedRooms);

				String availableRoomsJSON = gson.toJson(availabeRooms);
				request.setAttribute("bookedRooms", bookedRoomsJSON);
				request.setAttribute("availableRooms", availableRoomsJSON);
			}

			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(RESULT_PAGE);
			dispatcher.forward(request, response);
		} catch (ServletException | NumberFormatException e) {
			LOGGER.log(Level.ERROR, e.getMessage());
		}
	}

	@Override
	public void destroy() {
		super.destroy();
		((ClassPathXmlApplicationContext) applicationContext).close();
	}
}
