package com.epam.booking.dto;

import java.util.Set;

public class GuestDTO {
	private int id;
	private String name;
	private int passportId;
	
	private Set<BookedRoomDTO> rooms;
	
	public GuestDTO() {
		super();
	}

	public GuestDTO(int id, String name, int passportId) {
		super();
		this.id = id;
		this.name = name;
		this.passportId = passportId;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPassportId() {
		return passportId;
	}
	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}
	
	public Set<BookedRoomDTO> getRooms() {
		return rooms;
	}

	public void setRooms(Set<BookedRoomDTO> rooms) {
		this.rooms = rooms;
	}
	
	@Override
	public String toString() {
		return "GuestDTO [id=" + id + ", name=" + name + ", passportId=" + passportId + ", rooms=" + rooms + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + passportId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuestDTO other = (GuestDTO) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (passportId != other.passportId)
			return false;
		return true;
	}


}
