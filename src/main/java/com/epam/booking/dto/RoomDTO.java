package com.epam.booking.dto;

import java.util.Set;

public class RoomDTO {
	private int roomNumber;
	private int price;
	
	private Set<BookedRoomDTO> bookedRooms;

	public RoomDTO() {
		super();
	}

	public RoomDTO(int roomNumber, int price) {
		super();
		this.roomNumber = roomNumber;
		this.price = price;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Set<BookedRoomDTO> getBookedRooms() {
		return bookedRooms;
	}

	public void setBookedRooms(Set<BookedRoomDTO> bookedRooms) {
		this.bookedRooms = bookedRooms;
	}

	@Override
	public String toString() {
		return "RoomDTO [roomNumber=" + roomNumber + ", price=" + price + ", bookedRooms=" + bookedRooms + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + price;
		result = prime * result + roomNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoomDTO other = (RoomDTO) obj;
		if (price != other.price)
			return false;
		if (roomNumber != other.roomNumber)
			return false;
		return true;
	}
	
	
	
}
