package com.epam.booking.entity;

import java.sql.Date;


public class BookedRoom {
	private int id;
	private Guest guest;
	private Room room;
	private Date bookingDate;
	private Date expiredDate;
	
	public BookedRoom() {
		guest = new Guest();
		room = new Room();
	}
	
	public BookedRoom(int id, Guest guest, Room room, Date bookingDate, Date expiredDate) {
		super();
		this.id = id;
		this.guest = guest;
		this.room = room;
		this.bookingDate = bookingDate;
		this.expiredDate = expiredDate;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Guest getGuest() {
		return guest;
	}
	public void setGuest(Guest guest) {
		this.guest = guest;
	}
	
	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Date getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}
	public Date getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	@Override
	public String toString() {
		return "BookedRoom [id=" + id + ", guest=" + guest.getId() + ", room=" + room.getRoomNumber() + ", bookingDate=" + bookingDate
				+ ", expiredDate=" + expiredDate + "]";
	}
	
	public String toFullString() {
		return "BookedRoom [id=" + id + ", guest=" + guest.toString() + ", room=" + room.toString() + ", bookingDate=" + bookingDate
				+ ", expiredDate=" + expiredDate + "]";
	}
	
	
}
