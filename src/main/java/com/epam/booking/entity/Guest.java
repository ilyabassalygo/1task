package com.epam.booking.entity;

import java.util.HashSet;
import java.util.Set;


public class Guest {
	private int id;
	private String name;
	private int passportId;
	
	private Set<BookedRoom> rooms = new HashSet<BookedRoom>(0);
	
	public Guest() {
	}
	
	public Guest(int id, String name, int passportId) {
		super();
		this.id = id;
		this.name = name;
		this.passportId = passportId;
	}

	public Guest(int id, String name, int passportId, Set<BookedRoom> rooms) {
		super();
		this.id = id;
		this.name = name;
		this.passportId = passportId;
		this.rooms = rooms;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPassportId() {
		return passportId;
	}

	public void setPassportId(int passportId) {
		this.passportId = passportId;
	}

	public Set<BookedRoom> getRooms() {
		return rooms;
	}

	public void setRooms(Set<BookedRoom> rooms) {
		this.rooms = rooms;
	}

	@Override
	public String toString() {
		return "Guest [id=" + id + ", name=" + name + ", passportId=" + passportId + ", rooms=" + rooms + "]";
	}

}
