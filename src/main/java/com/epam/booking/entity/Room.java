package com.epam.booking.entity;

import java.util.HashSet;
import java.util.Set;


public class Room {
	private int roomNumber;
	private int price;
	
	private Set<BookedRoom> bookedRooms = new HashSet<>(0);
	
	public Room() {
	}
	
	public Room(int roomNumber, int price) {
		super();
		this.roomNumber = roomNumber;
		this.price = price;
	}

	public Room(int roomNumber, int price, Set<BookedRoom> bookedRooms) {
		super();
		this.roomNumber = roomNumber;
		this.price = price;
		this.bookedRooms = bookedRooms;
	}

	public int getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Set<BookedRoom> getBookedRooms() {
		return bookedRooms;
	}

	public void setBookedRooms(Set<BookedRoom> bookedRooms) {
		this.bookedRooms = bookedRooms;
	}

	@Override
	public String toString() {
		return "Room [roomNumber=" + roomNumber + ", price=" + price + ", bookedRooms=" + bookedRooms.toString() + "]";
	}
		
}
