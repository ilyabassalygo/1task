package test.com.epam.booking.dao;

import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.booking.dao.RoomDAO;
import com.epam.booking.entity.Room;

public class RoomDAOTest extends DBUnitConfig {
	private final static String DATA_SET_XML_PATH = "com/epam/booking/entity/guest/";
	private final static String CONTEXT_PATH = "test-spring-config.xml";
	private final static String ROOM_TABLE = "ROOM";

	private ApplicationContext applicationContext;
	private RoomDAO roomDAO;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder()
				.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(DATA_SET_XML_PATH + "guest-data.xml"));
		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	public RoomDAOTest() {
	}
	
	private ITable getData(String fileName,String tableName) throws DataSetException {
		IDataSet data = new FlatXmlDataSetBuilder()
				.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(DATA_SET_XML_PATH + fileName));
		ITable returnData = data.getTable(tableName);
		return returnData;
	}

	@Test
	public void testFindAll() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		roomDAO = applicationContext.getBean(RoomDAO.class);

		ITable expected = getData("guest-data.xml", ROOM_TABLE);
		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(ROOM_TABLE);
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void findTest() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		roomDAO = applicationContext.getBean(RoomDAO.class);
		Room actualRoom = roomDAO.find(1);
		Room expectedRoom = new Room();
		expectedRoom.setRoomNumber(1);
		expectedRoom.setPrice(1111);

		Assert.assertEquals(expectedRoom, actualRoom);
	
	}

	@Test
	public void testSave() throws SQLException, Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		roomDAO = applicationContext.getBean(RoomDAO.class);
		Room room = new Room();
		room.setRoomNumber(4);
		room.setPrice(4444);
		
		roomDAO.save(room);
		ITable expected = getData("guest-data-save.xml", ROOM_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(ROOM_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdate() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		roomDAO = applicationContext.getBean(RoomDAO.class);
		Room room = new Room();
		room.setRoomNumber(3);
		room.setPrice(4444);

		roomDAO.update(room);

		ITable expected = getData("guest-data-update.xml", ROOM_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(ROOM_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void testRemove() throws Exception {

		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		roomDAO = applicationContext.getBean(RoomDAO.class);

		roomDAO.remove(3);

		ITable expected = getData("guest-data-remove.xml", ROOM_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(ROOM_TABLE);
		
		Assertion.assertEquals(expected, actual);
		
	}
}
