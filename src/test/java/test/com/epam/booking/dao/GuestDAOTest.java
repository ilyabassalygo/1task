package test.com.epam.booking.dao;

import java.sql.SQLException;

import org.dbunit.Assertion;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.booking.dao.GuestDAO;
import com.epam.booking.entity.Guest;

public class GuestDAOTest extends DBUnitConfig {
	private final static String DATA_SET_XML_PATH = "com/epam/booking/entity/guest/";
	private final static String CONTEXT_PATH = "test-spring-config.xml";
	private final static String GUEST_TABLE = "GUEST";

	private ApplicationContext applicationContext;
	private GuestDAO guestDAO;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder()
				.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(DATA_SET_XML_PATH + "guest-data.xml"));
		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	public GuestDAOTest() {
	}
	
	private ITable getData(String fileName,String tableName) throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		IDataSet data = new FlatXmlDataSetBuilder()
				.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(DATA_SET_XML_PATH + fileName));
		ITable returnData = data.getTable(tableName);
		return returnData;
	}

	@Test
	public void testFindAll() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		guestDAO = applicationContext.getBean(GuestDAO.class);

		ITable expected = getData("guest-data.xml", GUEST_TABLE);
		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(GUEST_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void findTest() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		guestDAO = applicationContext.getBean(GuestDAO.class);
		Guest actualGuest = guestDAO.find(1);
		Guest expectedGuest = new Guest();
		expectedGuest.setId(1);
		expectedGuest.setName("first");
		expectedGuest.setPassportId(1111);

		Assert.assertEquals(expectedGuest, actualGuest);
		
	}

	@Test
	public void testSave() throws SQLException, Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		guestDAO = applicationContext.getBean(GuestDAO.class);
		Guest guest = new Guest();
		guest.setId(4);
		guest.setName("fourthTest");
		guest.setPassportId(4444);
		
		guestDAO.save(guest);
		ITable expected = getData("guest-data-save.xml", GUEST_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(GUEST_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdate() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		guestDAO = applicationContext.getBean(GuestDAO.class);
		Guest guest = new Guest();
		guest.setId(3);
		guest.setName("fourthTest");
		guest.setPassportId(4444);
		
		guestDAO.update(guest);

		ITable expected = getData("guest-data-update.xml", GUEST_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(GUEST_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void testRemove() throws Exception {

		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		guestDAO = applicationContext.getBean(GuestDAO.class);
		guestDAO.remove(3);

		ITable expected = getData("guest-data-remove.xml", GUEST_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(GUEST_TABLE);
		
		Assertion.assertEquals(expected, actual);
		
	}

}
