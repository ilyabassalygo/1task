package test.com.epam.booking.dao;

import org.dbunit.DBTestCase;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;

public class DBUnitConfig extends DBTestCase {
	private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String CONNECTION = "jdbc:oracle:thin:test@//localhost:1521/xe";
	private static final String USERNAME = "TEST";
	private static final String PASSWORD = "11223344";
	private static final String SCHEME = "TEST";
	
	
    protected IDatabaseTester tester;
    protected IDataSet beforeData;
    protected DatabaseConfig databaseConfig;
    
    @Before
    public void setUp() throws Exception {
        tester = new JdbcDatabaseTester(DRIVER, CONNECTION, USERNAME, PASSWORD, SCHEME);
    }
 
    public DBUnitConfig() {
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS,
                DRIVER);
            System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL,
                CONNECTION);
            System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME,
                USERNAME);
            System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD,
               PASSWORD);
            System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA,
                SCHEME);
	}

	@Override
    protected IDataSet getDataSet() throws Exception {
        return beforeData;
    }
 
    @Override
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.DELETE_ALL;
    }
 
}