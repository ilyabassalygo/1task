package test.com.epam.booking.dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import org.dbunit.Assertion;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.booking.dao.BookedRoomDAO;
import com.epam.booking.entity.BookedRoom;
import com.epam.booking.entity.Guest;
import com.epam.booking.entity.Room;

public class BookedRoomDAOTest extends DBUnitConfig {
	private final static String DATA_SET_XML_PATH = "com/epam/booking/entity/guest/";
	private final static String CONTEXT_PATH = "test-spring-config.xml";
	private final static String BOOKED_ROOM_TABLE = "Booked_Room";

	private ApplicationContext applicationContext;
	private BookedRoomDAO bookedRoomDAO;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		beforeData = new FlatXmlDataSetBuilder()
				.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(DATA_SET_XML_PATH + "guest-data.xml"));
		tester.setDataSet(beforeData);
		tester.onSetup();
	}

	public BookedRoomDAOTest() {
	}
	
	private ITable getData(String fileName,String tableName) throws DataSetException {
		IDataSet data = new FlatXmlDataSetBuilder()
				.build(Thread.currentThread().getContextClassLoader().getResourceAsStream(DATA_SET_XML_PATH + fileName));
		ITable returnData = data.getTable(tableName);
		return returnData;
	}

	@Test
	public void testFindAll() throws Exception {

		ITable expected = getData("guest-data.xml", BOOKED_ROOM_TABLE);
		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(BOOKED_ROOM_TABLE);

		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		bookedRoomDAO = applicationContext.getBean(BookedRoomDAO.class);
		
		List<BookedRoom> rooms = bookedRoomDAO.findAll();

		Assertion.assertEquals(expected, actual);
		Assert.assertEquals(actual.getRowCount(), rooms.size());
	}
	
	@Test
	public void findTest() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		bookedRoomDAO = applicationContext.getBean(BookedRoomDAO.class);
		BookedRoom actualRoom = bookedRoomDAO.find(1);
		BookedRoom expectedRoom = new BookedRoom();
		expectedRoom.setId(1);
		
		Guest guest = new Guest();
		guest.setId(1);
		guest.setName("test");
		guest.setPassportId(1111);
		expectedRoom.setGuest(guest);
		
		Room room = new Room();
		room.setRoomNumber(1);
		room.setPrice(1111);
		expectedRoom.setRoom(room);
		expectedRoom.setBookingDate(Date.valueOf("2017-12-31 22:33:33"));
		expectedRoom.setExpiredDate(Date.valueOf("2017-12-31 22:33:33"));

		Assert.assertEquals(expectedRoom, actualRoom);
		
	}

	@Test
	public void testSave() throws SQLException, Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		bookedRoomDAO = applicationContext.getBean(BookedRoomDAO.class);
		BookedRoom bookedRoom = new BookedRoom();
		bookedRoom.setId(3);
		
		Guest guest = new Guest();
		guest.setId(3);
		guest.setName("thirdTest");
		guest.setPassportId(3333);
		bookedRoom.setGuest(guest);
		
		Room room = new Room();
		room.setRoomNumber(3);
		room.setPrice(3333);
		bookedRoom.setRoom(room);
		bookedRoom.setBookingDate(Date.valueOf("2017-12-31"));
		bookedRoom.setExpiredDate(Date.valueOf("2017-12-31"));
		
		bookedRoomDAO.save(bookedRoom);
		ITable expected = getData("guest-data-save.xml", BOOKED_ROOM_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(BOOKED_ROOM_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdate() throws Exception {
		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		bookedRoomDAO = applicationContext.getBean(BookedRoomDAO.class);
		BookedRoom bookedRoom = new BookedRoom();
		bookedRoom.setId(2);
		
		Guest guest = new Guest();
		guest.setId(3);
		guest.setName("thirdTest");
		guest.setPassportId(3333);
		bookedRoom.setGuest(guest);
		
		Room room = new Room();
		room.setRoomNumber(3);
		room.setPrice(3333);
		bookedRoom.setRoom(room);
		bookedRoom.setBookingDate(Date.valueOf("2017-12-31"));
		bookedRoom.setExpiredDate(Date.valueOf("2017-12-31"));

		bookedRoomDAO.update(bookedRoom);

		ITable expected = getData("guest-data-update.xml", BOOKED_ROOM_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(BOOKED_ROOM_TABLE);
		
		Assertion.assertEquals(expected, actual);
	}
	
	@Test
	public void testRemove() throws Exception {

		applicationContext = new ClassPathXmlApplicationContext(CONTEXT_PATH);
		bookedRoomDAO = applicationContext.getBean(BookedRoomDAO.class);

		bookedRoomDAO.remove(2);

		ITable expected = getData("guest-data-remove.xml", BOOKED_ROOM_TABLE);

		IDataSet actualData = tester.getConnection().createDataSet();
		ITable actual = actualData.getTable(BOOKED_ROOM_TABLE);
		
		Assertion.assertEquals(expected, actual);
		
	}

}
