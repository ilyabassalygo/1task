package test.com.epam.booking.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.epam.booking.dao.GuestDAO;
import com.epam.booking.dto.GuestDTO;
import com.epam.booking.entity.Guest;
import com.epam.booking.service.GuestService;
import com.epam.booking.service.GuestServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class GuestServiceTest {

	@InjectMocks
	GuestService guestService = new GuestServiceImpl();

	@Mock
	GuestDAO guestDAO;
	
	List<Guest> guests;
	
	@Before
	public void init() {
		guests = new ArrayList<>();
		Guest guest1 = new Guest(1,"first",1);
		Guest guest2 = new Guest(2,"second",2);
		Guest guest3 = new Guest(3,"third",3);
		
		guests.add(guest1);
		guests.add(guest2);
		guests.add(guest3);
		
	}
	
	@Test
	public void testFindAll() throws Exception {
		List<GuestDTO> expected = new ArrayList<>();
		GuestDTO guestDTO1 = new GuestDTO(1,"first",1);
		GuestDTO guestDTO2 = new GuestDTO(2,"second",2);
		GuestDTO guestDTO3 = new GuestDTO(3,"third",3);
		
		expected.add(guestDTO1);
		expected.add(guestDTO2);
		expected.add(guestDTO3);
		
		Mockito.when(guestDAO.findAll()).thenReturn(guests);
		
		List<GuestDTO> actual = guestService.findaAll();
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testFindGuest() throws Exception {
		GuestDTO expected = new GuestDTO(1,"first",1);
		Mockito.when(guestDAO.find(1)).thenReturn(new Guest(1,"first",1));
		GuestDTO actual = guestService.findGuest(1);
		Assert.assertEquals(expected, actual);		
	}
}
