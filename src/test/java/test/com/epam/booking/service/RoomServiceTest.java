package test.com.epam.booking.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.epam.booking.dao.RoomDAO;
import com.epam.booking.dto.BookedRoomDTO;
import com.epam.booking.dto.RoomDTO;
import com.epam.booking.entity.BookedRoom;
import com.epam.booking.entity.Room;
import com.epam.booking.service.RoomService;
import com.epam.booking.service.RoomServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class RoomServiceTest {
	//@InjectMocks shows where mocks will be injected
	@InjectMocks
	RoomService roomService = new RoomServiceImpl();

	@Mock
	RoomDAO roomDAO;

	List<Room> rooms;
	
	@Before
	public void init() {
		rooms = new ArrayList<>();
		Set<BookedRoom> bookedRooms = new HashSet<>();
		bookedRooms.add(new BookedRoom());
		Room room1 = new Room(1, 1);
		room1.setBookedRooms(bookedRooms);
		Room room2 = new Room(2, 2);
		Room room3 = new Room(3, 3);
		rooms.add(room1);
		rooms.add(room2);
		rooms.add(room3);
	}
	
	@Test
	public void testFindAllRooms() throws Exception {
		List<RoomDTO> expectedRooms = new ArrayList<>();
		RoomDTO room1 = new RoomDTO(1, 1);
		RoomDTO room2 = new RoomDTO(2, 2);
		RoomDTO room3 = new RoomDTO(3, 3);
		expectedRooms.add(room1);
		expectedRooms.add(room2);
		expectedRooms.add(room3);
		
		Mockito.when(roomDAO.findAll()).thenReturn(rooms);

		List<RoomDTO> actualRooms = roomService.findAllRooms();
		Assert.assertEquals(expectedRooms, actualRooms);
	}

	@Test
	public void testFindBookedRooms() throws Exception {
		List<RoomDTO> expectedRooms = new ArrayList<>();
		
		RoomDTO room1 = new RoomDTO(1, 1);
		Set<BookedRoomDTO> bookedRooms = new HashSet<>();
		bookedRooms.add(new BookedRoomDTO());
		room1.setBookedRooms(bookedRooms);
		
		expectedRooms.add(room1);

		Mockito.when(roomDAO.findAll()).thenReturn(rooms);
		List<RoomDTO> actualRooms = roomService.findBookedRooms();
		Assert.assertEquals(expectedRooms, actualRooms);
	}
	
	@Test
	public void testFindNotBookedRooms() throws Exception {
		List<RoomDTO> expectedRooms = new ArrayList<>();
		
		RoomDTO room2 = new RoomDTO(2, 2);
		RoomDTO room3 = new RoomDTO(3, 3);

		expectedRooms.add(room2);
		expectedRooms.add(room3);
		
		Mockito.when(roomDAO.findAll()).thenReturn(rooms);
			
		List<RoomDTO> actualRooms = roomService.findNotBookedRooms();
		Assert.assertEquals(expectedRooms, actualRooms);
		
	}
	
	@Test
	public void testFindByPrice() throws Exception {
		List<RoomDTO> expectedRooms = new ArrayList<>();
		RoomDTO room1 = new RoomDTO(1, 1);
		RoomDTO room2 = new RoomDTO(2, 2);
		RoomDTO room3 = new RoomDTO(3, 3);
		expectedRooms.add(room1);
		expectedRooms.add(room2);
		expectedRooms.add(room3);

		Mockito.when(roomDAO.findAll()).thenReturn(rooms);

		List<RoomDTO> actualRooms = roomService.findByPrice(1);
		Assert.assertEquals(expectedRooms, actualRooms);
	}
	
	@Test
	public void testFindByNumber() throws Exception {
		Mockito.when(roomDAO.find(1)).thenReturn(new Room(1,1));
		RoomDTO expected = new RoomDTO(1,1);
		RoomDTO actual = roomService.findByNumber(1);
		
		Assert.assertEquals(expected, actual);
	}
}
